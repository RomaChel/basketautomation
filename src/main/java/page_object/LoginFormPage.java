package page_object;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class LoginFormPage extends BasePage {


    public LoginFormPage(WebDriver driver) {
        super(driver);
    }

    private By userName() {
        return By.id("username");
    }

    private By password() {
        return By.id("password");
    }

    private By submitBtn() {
        return By.xpath("//button[@type='submit']");
    }

    private By errorTooltip() {
       return By.cssSelector(".flash.error");
    }

    private By successTooltip() {
        return By.cssSelector(".flash.success");
    }

    //------------------------------------------------------------------------------------------------------------------

    public LoginFormPage enterUserName(String userName) {
        driver.findElement(userName()).sendKeys(userName);
        return this;
    }

    public LoginFormPage enterPassword(String password) {
        driver.findElement(password()).sendKeys(password);
        return this;
    }

    public SecurePage login(String userName, String password) {
        enterUserName(userName)
                .enterPassword(password)
                .driver.findElement(submitBtn()).click();
        return new SecurePage(this.driver);
    }

    public String getMessageFromTooltip(boolean success) {
        if (success) {
            return driver.findElement(successTooltip()).getText().replaceAll("×", "").trim();
        } else {
            return driver.findElement(errorTooltip()).getText().replaceAll("×", "").trim();
        }
    }
}
