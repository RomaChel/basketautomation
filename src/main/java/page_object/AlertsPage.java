package page_object;

import enum_from_page.AlertButtons;
import org.openqa.selenium.*;
import org.openqa.selenium.support.ui.ExpectedCondition;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;

public class AlertsPage extends BasePage {

    public AlertsPage(WebDriver driver) {
        super(driver);
    }

    private By alertsBtn(String nameButton) {
        return By.xpath("//button[contains(text(),'" + nameButton + "')]");
    }

    private By resultText() {
        return By.cssSelector("#result");
    }

//----------------------------------------------------------------------------------------------------------------------

    public void clickOnButton(String button) {
        System.out.println(button);
        WebElement element = (new WebDriverWait(driver, 10)).until(ExpectedConditions
                .presenceOfElementLocated(alertsBtn(button)));
        element.click();
    }

    public void clickOnButtonUsingJS(AlertButtons button) {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("return " + button.getOnClickJSFunction());
    }

    public void clickOnButtonWithJS(AlertButtons button) {
        WebElement buttonToClick = driver.findElement(alertsBtn(button.getTextOnButton()));
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("return arguments[0].click()", buttonToClick);
    }

    public String switchToAlertAndGetText(boolean confirm, String... messages) {
        Alert alert = driver.switchTo().alert();
        String alertText = alert.getText();
        if (messages.length > 0) {
            alert.sendKeys(messages[0]);
        }
        if (confirm) {
            alert.accept();
        } else {
            alert.dismiss();
        }
        return alertText;
    }

    public String getResultText() {
        return driver.findElement(resultText()).getText();
    }

}
