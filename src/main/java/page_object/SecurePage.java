package page_object;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;

public class SecurePage extends BasePage {
    public SecurePage(WebDriver driver) {
        super(driver);
    }

    private By successTooltip() {
        return By.cssSelector(".flash.success");
    }

    private By logOutBtn() {
        return By.xpath("//a[@href='/logout']/i");
    }

    private By titleOfPage() {
        return  By.xpath("//h2");
    }

    //------------------------------------------------------------------------------------------------------------------

    public SecurePage checkSuccessTooltip(String expectedText) {
        driver.findElement(successTooltip()).isDisplayed();
        Assert.assertEquals(driver.findElement(successTooltip()).getText().replaceAll("×", "").trim(), expectedText);
        return this;
    }

    public LoginFormPage logout() {
        driver.findElement(logOutBtn()).click();
        return new LoginFormPage(this.driver);
    }

    public SecurePage checkTitleOfPage(String expectedTitle) {
        Assert.assertEquals(driver.findElement(titleOfPage()).getText(), expectedTitle);
        return this;
    }
}
