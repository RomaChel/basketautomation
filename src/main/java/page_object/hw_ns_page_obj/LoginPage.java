package page_object.hw_ns_page_obj;


import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import page_object.BasePage;



public class LoginPage extends BasePage {

    public LoginPage(WebDriver driver) {
        super(driver);
    }

    private final By btnFormAuthentication = By.xpath("//a[@href='/login']");
    private final By inputUsername = By.xpath("//input[@name='username']");
    private final By inputPassword = By.xpath("//input[@name='password']");
    private final By btnLogIn = By.xpath("//button[@type='submit']");




    public LoginPage clickBtnFormAuthentication(){
        driver.findElement(btnFormAuthentication).click();
        return this;
    }

    public LoginPage writeUsername(){
        driver.findElement(inputUsername).sendKeys("tomsmith");
        return this;
    }

    public LoginPage writePassword(){
        driver.findElement(inputPassword).sendKeys("SuperSecretPassword!");
        return this;
    }

    public LoginPage clickBtnLogIn(){
        driver.findElement(btnLogIn).click();
        return this;
    }

}
