package page_object.hw_ns_page_obj;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import page_object.BasePage;

public class LogOutPage extends BasePage {
    public LogOutPage(WebDriver driver) {
        super(driver);
    }

    private final By btnLogout = By.xpath("//a[@href='/logout']");

    public LogOutPage clickBtnLogout() {
        driver.findElement(btnLogout).click();
        return this;
    }
}
