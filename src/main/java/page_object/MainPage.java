package page_object;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;

public class MainPage extends BasePage {

    public MainPage(WebDriver driver) {
        super(driver);
    }

/*    @FindBy(xpath = "//a[@href='/javascript_alerts']")
    private WebElement alertsBtn;*/

    private By btnFromPage(String nameButton) {
        return By.xpath("//a[@href='/" + nameButton + "']");
    }
    //------------------------------------------------------------------------------------------------------------------

    public void clickOnBtnFromPage(String nameButton) {
        JavascriptExecutor javascriptExecutor = (JavascriptExecutor) driver;
        javascriptExecutor.executeScript("arguments[0].scrollIntoView(true);", driver.findElement(btnFromPage(nameButton)));
        driver.findElement(btnFromPage(nameButton)).click();
    }
}