package enum_from_page;

public enum AlertButtons {

    ALERT("JS Alert", "jsAlert()"),
    CONFIRM("JS Confirm", "jsConfirm()"),
    PROMPT("JS Prompt", "jsPrompt()");

    private final String textOnButton, onClickJSFunction;

    AlertButtons(String textOnButton, String onClickJSFunction) {
        this.textOnButton = textOnButton;
        this.onClickJSFunction = onClickJSFunction;
    }

    public String getTextOnButton() {
        return textOnButton;
    }

    public String getOnClickJSFunction() {
        return onClickJSFunction;
    }
}

