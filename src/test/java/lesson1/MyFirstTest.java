package lesson1;

import io.qameta.allure.Description;
import org.testng.annotations.*;


public class MyFirstTest {

    @BeforeClass
    public void beforeClass() {
        System.out.println("Our before class");
    }

    @BeforeTest
    public void beforeTest() {
        System.out.println("Our before test");
    }

    @BeforeMethod
    public void beforeMethod() {
        System.out.println("Our before each test");
    }

    @DataProvider(name = "myDP")
    public Object[][] dataProvider() {
        return new Object[][] {
                {"qwe@gmail.com", "qwe"},
                {"asd@gmail.com", "asd"},
                {"zxc@gmail.com", "zxc"}
        };

    }


    @Test(dataProvider = "myDP", priority = 2)
    @Description("Running first test")
    public void firstTest(String login, String pass) {
        System.out.println(login);
        System.out.println(pass);

    }

    @Test(priority = 3, enabled = false)
    public void secondTest() {
        System.out.println("It's second test");
    }

    @Test(priority = 1, invocationCount = 5)
    public void thirdTest() {
        System.out.println("It's third test");
    }


    @AfterMethod
    public void afterMethod() {
        System.out.println("after each test");
    }

    @AfterTest
    public void afterTest() {
        System.out.println("Our after test");
    }

}
