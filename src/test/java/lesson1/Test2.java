package lesson1;

import lesson1.listener.MyListener;
import org.testng.annotations.Listeners;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;
/*@Listeners(MyListener.class)*/

public class Test2 {

    @Test()
    @Parameters({"username", "password"})
    public void Test2(String username, String password) {
        System.out.println("This test 2");
        System.out.println(username + " " + password);
    }
}
