package lesson4;

import basetest.BaseTest;
import org.openqa.selenium.Cookie;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import page_object.LoginFormPage;
import page_object.MainPage;
import page_object.SecurePage;

import java.util.Set;

public class TestCookie extends BaseTest {

    LoginFormPage loginFormPage;
    SecurePage securePage;
    MainPage mainPage;
    String userName = "tomsmith";
    String password = "SuperSecretPassword!";
    String expectedTooltipText = "You logged into a secure area!";
    String expectedErrorTooltipText = "You must login to view the secure area!";
    String expectedTitle = "Secure Area";


    @BeforeClass
    public void beforeClass() {
        openUrl("https://the-internet.herokuapp.com");
        mainPage = new MainPage(driver);
        loginFormPage = new LoginFormPage(driver);
        securePage = new SecurePage(driver);
    }

    @Test
    public void cookieTest() {
        mainPage.clickOnBtnFromPage("login");
        loginFormPage.login(userName, password)
                .checkSuccessTooltip(expectedTooltipText);
        Set<Cookie> cookies = driver.manage().getCookies();
        for (Cookie cookie : cookies) {
            System.out.println(cookie.getName() + " " + cookie.getValue() + " " + cookie.getExpiry());
        }

        System.out.println();

        Cookie myCookie = new Cookie("MyCookie", "MyValue");
        driver.manage().addCookie(myCookie);
        cookies = driver.manage().getCookies();
        for (Cookie cookie : cookies) {
            System.out.println(cookie.getName() + " " + cookie.getValue() + " " + cookie.getExpiry());
        }

        System.out.println();

        driver.manage().deleteCookieNamed(myCookie.getName());
        cookies = driver.manage().getCookies();
        for (Cookie cookie : cookies) {
            System.out.println(cookie.getName() + " " + cookie.getValue() + " " + cookie.getExpiry());
        }

        driver.navigate().refresh();
        securePage.checkTitleOfPage(expectedTitle);

        driver.manage().deleteAllCookies();
        driver.navigate().refresh();

        Assert.assertEquals(loginFormPage.getMessageFromTooltip(false), expectedErrorTooltipText);
    }
}
