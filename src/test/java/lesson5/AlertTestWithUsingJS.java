package lesson5;

import basetest.BaseTest;
import enum_from_page.AlertButtons;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import page_object.AlertsPage;
import page_object.MainPage;

public class AlertTestWithUsingJS extends BaseTest {

    AlertsPage alertsPage;

    @BeforeMethod
    public void beforeMethod() {
        openUrl("https://the-internet.herokuapp.com");
        alertsPage = new AlertsPage(driver);
    }

    @Test
    public void alertTestWithUsingJS() {
        new MainPage(driver).clickOnBtnFromPage("javascript_alerts");
        alertsPage.clickOnButtonUsingJS(AlertButtons.ALERT);
        Assert.assertEquals(alertsPage.switchToAlertAndGetText(true), "I am a JS Alert");
        Assert.assertEquals(alertsPage.getResultText(), "You successfully clicked an alert");
    }
}