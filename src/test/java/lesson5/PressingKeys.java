package lesson5;

import basetest.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;
import page_object.MainPage;

public class PressingKeys extends BaseTest {

    @BeforeMethod
    public void beforeMethod() {
        openUrl("https://the-internet.herokuapp.com");
    }

    @Test
    public void testForPressingKeys() {
        new MainPage(driver).clickOnBtnFromPage("key_presses");
        WebElement input = driver.findElement(By.xpath("//input[@id='target']"));
        WebElement result = driver.findElement(By.xpath("//p[@id='result']"));

        input.sendKeys("M");
        Assert.assertTrue(result.getText().contains("M"));
        input.sendKeys(Keys.BACK_SPACE);
        Assert.assertTrue(result.getText().contains("BACK_SPACE"));
    }
}
