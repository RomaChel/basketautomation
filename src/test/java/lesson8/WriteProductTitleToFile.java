package lesson8;

import basetest.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WriteProductTitleToFile extends BaseTest {

    private File productTitle = new File("C:\\Work\\test_example\\result.txt");

    @BeforeClass
    public void beforeClass() {
        openUrl("https://www.ctrs.com.ua/");
    }

    @Test
    public void writeProductTitleToFile() {
        driver.findElement(By.xpath("//div[text()='Смартфони']")).click();
        writeToFile(saveTitleProductToList());
    }

   //-------------------------------------------------------------------------------------------------------------------

    public ArrayList<String> saveTitleProductToList() {
        ArrayList<String> listOfProductTitle = new ArrayList<>();
        List<WebElement> productTitle = driver.findElements(By.xpath("//a[contains(@class,'break-word link')]"));
        if (productTitle.size() > 0) {
            for (WebElement product : productTitle) {
                String name = product.getText();
                listOfProductTitle.add(name + "\n");
            }
        }
        return listOfProductTitle;
    }

    public void writeToFile(ArrayList<String> list) {
        System.out.println(list);
        FileWriter fileWriter = null;
        try {
        if (!productTitle.exists()) {
            productTitle.createNewFile();
        }
            fileWriter = new FileWriter(productTitle);
        for (String item : list) {
            fileWriter.write(item);
        }
            fileWriter.close();
            System.out.println("File writing done!");
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
