package basetest;

import io.github.bonigarcia.wdm.managers.ChromeDriverManager;
import org.driver.Browsers;
import org.driver.WebDriverFactory;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.testng.annotations.AfterSuite;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeSuite;
import org.testng.annotations.BeforeTest;

import java.util.concurrent.TimeUnit;

import static java.lang.Thread.sleep;

public class BaseTest {

    protected WebDriver driver = null;

    @BeforeTest
    public void setUpBrowser() {
       /* ChromeDriverManager.getInstance().setup();
        driver = new ChromeDriver();*/
        driver = WebDriverFactory.initDriver();
        driver.manage().window().maximize();
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        driver.manage().timeouts().pageLoadTimeout(30, TimeUnit.SECONDS);
    }

    @AfterTest
    public void closeDriver() {
        if (driver != null) {
            driver.quit();
        }
    }

    public void openUrl(String url) {
        driver.navigate().to(url);
    }

    public void pause(long msec) {
        try {
            sleep(msec);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }
}
