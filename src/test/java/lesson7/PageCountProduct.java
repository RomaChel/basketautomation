package lesson7;

import basetest.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.List;

import static org.testng.Assert.*;

public class PageCountProduct extends BaseTest {

    @BeforeClass
    public void beforeClass() {
        openUrl("https://www.ctrs.com.ua/");
    }

    @Test
    public void pageCountProduct() {
        driver.findElement(By.xpath("//div[text()='Смартфони']")).click();
        List<WebElement> productCards = driver.findElements(By.xpath("//div[contains(@class,'productCardCategory')]"));
        productCards.get(0).isDisplayed();
        assertEquals(productCards.size(), 47);
    }
}
