package lesson7;

import basetest.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;


public class CheckSortingByPrice extends BaseTest {

    private List<WebElement> price() {
        return driver.findElements(By.xpath("//div[contains(@class,'medium')]"));
    }

    @BeforeClass
    public void beforeClass() {
        openUrl("https://www.ctrs.com.ua/smartfony/");
    }

    @Test
    public void testCheckSortingByPrice() {
        driver.findElement(By.xpath("//p[@class='line-clamp-1']/..")).click();
        driver.findElement(By.xpath("//a[@href='/smartfony/?order=price-asc']")).isDisplayed();
        driver.findElement(By.xpath("//a[@href='/smartfony/?order=price-asc']")).click();
        pause(5000);
        checkSortingByPrice(true);

        driver.findElement(By.xpath("//p[@class='line-clamp-1']/..")).click();
        driver.findElement(By.xpath("//a[@href='/smartfony/?order=price-desc']")).isDisplayed();
        driver.findElement(By.xpath("//a[@href='/smartfony/?order=price-desc']")).click();
        pause(5000);
        checkSortingByPrice(false);

    }




    //------------------------------------------------------------------------------------------------------------------

    public void checkSortingByPrice(boolean naturalOrder) {
        List<Double> priceFromSite = new ArrayList<>();
        for (WebElement element : price()) {
            String priceText = element.getText().replaceAll("\\D", "");
            System.out.println(priceText);
            priceFromSite.add(Double.valueOf(priceText));
        }
        List<Double> sortedPrice = new ArrayList<>(priceFromSite);
        if(naturalOrder==true) {
            Collections.sort(sortedPrice);
        } else {
            Collections.sort(sortedPrice, Collections.reverseOrder());
        }
        Assert.assertEquals(priceFromSite, sortedPrice);
    }

}
