package lesson6;

import basetest.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.Select;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import static org.testng.Assert.*;

public class TestForDropdownList extends BaseTest {

    @BeforeClass
    public void beforeClass() {
        openUrl("https://the-internet.herokuapp.com/dropdown");
    }

    @Test
    public void testForDropdownList() {
        WebElement option1 = driver.findElement(By.xpath("//select[@id='dropdown']/option[@value='1']"));
        WebElement option2 = driver.findElement(By.xpath("//select[@id='dropdown']/option[@value='2']"));

        Select dropdown = new Select(driver.findElement(By.id("dropdown")));
        dropdown.selectByVisibleText("Option 2");
        assertTrue(option2.isSelected());
        dropdown.selectByVisibleText("Option 1");
        assertTrue(option1.isSelected());
    }

}
