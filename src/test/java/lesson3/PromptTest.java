package lesson3;

import basetest.BaseTest;
import enum_from_page.AlertButtons;
import org.testng.Assert;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;
import page_object.AlertsPage;
import page_object.MainPage;

public class PromptTest extends BaseTest {

    AlertsPage alertsPage;

    @BeforeMethod
    public void beforeMethod() {
        openUrl("https://the-internet.herokuapp.com");
        alertsPage = new AlertsPage(driver);
    }

    @DataProvider(name = "testParam")
    Object[][] dataProvider() {
        return new Object[][] {
                {true, "Some test text!!!", "You entered: Some test text!!!"},
                {false, "Some test text!!!", "You entered: null"},
                {true, "", "You entered:"},
                {false, "", "You entered: null"}
        };
    }

    @Test(dataProvider = "testParam")
    public void testPrompt(boolean switcher, String testText, String expectedText) {
        new MainPage(driver).clickOnBtnFromPage("javascript_alerts");
        alertsPage.clickOnButton(AlertButtons.PROMPT.getTextOnButton());
        alertsPage.switchToAlertAndGetText(switcher, testText);
        Assert.assertEquals(alertsPage.getResultText(), expectedText);
    }
}
