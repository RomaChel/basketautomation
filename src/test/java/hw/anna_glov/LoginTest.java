package hw.anna_glov;

import basetest.BaseTest;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest {


    @BeforeClass
    public void beforeClass() {
        openUrl("https://the-internet.herokuapp.com/");
    }

    @Test
    public void loginTest() {
        openLoginForm();
        fillLogin();
        fillPassword();
        logIn();
        logOut();
    }
    private void openLoginForm() {
        driver.findElement(By.xpath("//a[@href='/login']")).click();
        Assert.assertTrue(driver.getCurrentUrl().contains("login"));
    }
    private void fillLogin() {
        driver.findElement(By.cssSelector("#username")).sendKeys("tomsmith");
    }
    private void fillPassword() {
        driver.findElement(By.cssSelector("#password")).sendKeys("SuperSecretPassword!");
        //driver.findElement(By.cssSelector("#password")).submit();
    }
    private void logIn() {
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        Assert.assertTrue(driver.findElement(By.xpath("//h2")).isDisplayed());
        Assert.assertEquals("Secure Area", driver.findElement(By.xpath("//h2")).getText());
    }
    private void logOut() {
        driver.findElement(By.xpath("//a[@href='/logout']")).click();
        Assert.assertTrue(driver.findElement(By.xpath("//div[@class='flash success']")).isDisplayed());
        //Assert.assertEquals("You logged out of the secure area!", driver.findElement(By.xpath("//div[@class='flash success']")).getText());
    }
}
