package hw.nikolay;

import basetest.BaseTest;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import page_object.hw_ns_page_obj.LogOutPage;

public class LogOutTest extends BaseTest {



    @BeforeClass
    public void beforeClass() {
       openUrl("https://the-internet.herokuapp.com/");
    }


    @Test
    public void loginTest() {
        LogOutPage logOutPage = new LogOutPage(driver);
        logOutPage.clickBtnLogout();
        Assert.assertEquals("Login Page", driver.findElement(By.xpath("//h2")).getText());

    }
}
