package hw.nikolay;


import basetest.BaseTest;
import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import page_object.BasePage;
import page_object.hw_ns_page_obj.LogOutPage;
import page_object.hw_ns_page_obj.LoginPage;

public class LoginTest extends BaseTest {


    @BeforeClass
    public void beforeClass() {
        openUrl("https://the-internet.herokuapp.com");
    }

    @Test
    public void loginTest(){
        LoginPage loginPage = new LoginPage(driver);
        loginPage.clickBtnFormAuthentication();
        Assert.assertTrue(driver.getCurrentUrl().contains("login"));
        loginPage.writeUsername();
        loginPage.writePassword();
        loginPage.clickBtnLogIn();
        Assert.assertEquals("Secure Area", driver.findElement(By.xpath("//h2")).getText());

        LogOutPage logOutPage = new LogOutPage(driver);
        logOutPage.clickBtnLogout();
        Assert.assertEquals("Login Page", driver.findElement(By.xpath("//h2")).getText());

    }
}
