package hw.olgagrygorieva;

import basetest.BaseTest;
import org.openqa.selenium.By;
import org.testng.Assert;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

public class LoginTest extends BaseTest {


    @BeforeClass
    public void beforeClass() {
        openUrl("https://the-internet.herokuapp.com/");
    }

    @Test
    public void loginTest() {
        driver.findElement(By.xpath("//a[@href='/login']")).click();
        Assert.assertTrue(driver.getCurrentUrl().contains("login"));
        driver.findElement(By.cssSelector("#username")).sendKeys("tomsmith");
        driver.findElement(By.cssSelector("#password")).sendKeys("SuperSecretPassword!");
        //driver.findElement(By.cssSelector("#password")).submit();
        driver.findElement(By.xpath("//button[@type='submit']")).click();
        Assert.assertTrue(driver.findElement(By.xpath("//h2")).isDisplayed());
        Assert.assertEquals("Secure Area", driver.findElement(By.xpath("//h2")).getText());
    }
}
