package lesson2;

import basetest.BaseTest;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
import page_object.LoginFormPage;
import page_object.MainPage;

public class LoginTest extends BaseTest {

    LoginFormPage loginFormPage;
    MainPage mainPage;
    String userName = "tomsmith";
    String password = "SuperSecretPassword!";
    String expectedTooltipText = "You logged into a secure area!";


    @BeforeClass
    public void beforeClass() {
        openUrl("https://the-internet.herokuapp.com/");
        mainPage = new MainPage(driver);
        loginFormPage = new LoginFormPage(driver);
    }

    @Test
    public void successfulLoginTest() {
        mainPage.clickOnBtnFromPage("login");
        loginFormPage.login(userName, password)
                .checkSuccessTooltip(expectedTooltipText);
    }
}
